<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TechNews - HTML and CSS Template</title>

    <!-- favicon -->
    <link href="assets/img/favicon.png" rel=icon>

    <!-- web-fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,500' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/mobile-menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/offcanvas.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <!-- Bootstrap -->
    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- font-awesome -->
    <link href="assets/fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
    <!-- Mobile Menu Style -->
    <link href="assets/css/mobile-menu.css" rel="stylesheet">

    <!-- Owl carousel -->
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/css/owl.theme.default.min.css" rel="stylesheet">
    <!-- Theme Style -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body id="page-top">
    <div id="main-wrapper">

        <div class="uc-mobile-menu-pusher">
            <div class="content-wrapper">
                <section id="header_section_wrapper" class="header_section_wrapper">
                    <div class="container">
                        <div class="header-section">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="left_section">
                                        <span class="date">
                                            {{ date('D') }} .
                                        </span>
                                        <!-- Date -->
                                        <span class="time">
                                            {{ date('d-F-Y') }}
                                        </span>
                                        <!-- Time -->
                                        <!-- Top Social Section -->
                                    </div>
                                    <!-- Left Header Section -->
                                </div>
                                <div class="col-md-4">
                                    <div class="logo">
                                        <a href="index.html"><img src="{{ asset('abc.png') }}" style="width:200px;" alt="Tech NewsLogo"></a>
                                    </div>
                                    <!-- Logo Section -->
                                </div>
                                <div class="col-md-4">
                                    <div class="right_section" >
                                        <ul class="nav navbar-nav">
                                            <li><a href="{{ route('portal.index') }}" style="z-index:99;" >Admin Page</a></li>
                                        </ul>
                                        <!-- Language Section -->
                                        <!-- Search Section -->
                                    </div>
                                    <!-- Right Header Section -->
                                </div>
                            </div>
                        </div>
                        <!-- Header Section -->


                        <!-- .navigation-section -->
                    </div>
                    <!-- .container -->
                </section>
                <!-- header_section_wrapper -->

<hr>
                <section id="category_section" class="category_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="category_section mobile">
                                    <!----article_title------>
                                    <div class="category_article_wrapper">
                                    @foreach($portal as $index => $portaldata)
                                    <?php 
                                    $dates = explode('-',$portaldata->portal_date);
                                    $date = $dates[2];
                                    $month = $dates[1];
                                    $year = $dates[0];
                                    $newdate= $date .'-'.$month.'-'.$year;
                                    ?>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="top_article_img">
                                                    <a ><img src="{{asset('storage/images/'.$portaldata->image_path)}}"  class="card-img-top">
                                                    </a>
                                                </div>
                                       
                                                <!----top_article_img------>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="category_article_title">
                                                    <h2><a >{{$portaldata->portal_title}}</a></h2>
                                                </div>
                                                <!----category_article_title------>
                                                <div class="category_article_date"><a >{{$newdate}}</a>, by: <a
                                                        >{{$portaldata->portal_writer}}</a></div>
                                                <!----category_article_date------>
                                                <div class="category_article_content">
                                                {{$portaldata->portal_detail}}
                                                </div>
                                                <!----category_article_content------>

                                                <!----media_social------>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                                <!-- Mobile News Section -->



                            </div>
                            <!-- Design News Section -->
                        </div>
                        <!-- Left Section -->


                        <!-- Popular News -->


                        <!-- Most Commented News -->


                        <!--Advertisement-->
                    </div>
                    <!-- Right Section -->

            </div>
            <!-- Row -->

        </div>
        <!-- Container -->

        </section>
        <!-- Category News Section -->



        <section id="footer_section" class="footer_section">

            <div class="footer_bottom_Section">
                <div class="container">
                    <div class="row">
                        <div class="footer">
                            <div class="col-sm-3">
                                <div class="social">
                                    <a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a>
                                    <!--Twitter-->
                                    <a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a>
                                    <!--Google +-->
                                    <a class="icons-sm inst-ic"><i class="fa fa-instagram"> </i></a>
                                    <!--Linkedin-->
                                    <a class="icons-sm tmb-ic"><i class="fa fa-tumblr"> </i></a>
                                    <!--Pinterest-->
                                    <a class="icons-sm rss-ic"><i class="fa fa-rss"> </i></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <p>&copy; Copyright 2019-ABC NEWS . Design by: <a
                                        href="https://uicookies.com">uiCookies</a>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <p>Your Trusted News</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- #content-wrapper -->

    </div>
    <!-- .offcanvas-pusher -->

    <a href="#" class="crunchify-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

   
</body>

</html>