@extends('portal.layout')

@section('content')
<style>

</style>
<div class="uper">

    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
    @endif
    <a href="{{ route('portal.create')}}" class="btn btn-success" style="margin:5px;">Create</a>
    <table class="table ">
        <thead>
            <tr>
                <td>No</td>
                <td>Stock Name</td>
                <td>Stock Price</td>
                <td>Stock Quantity</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($portal as $index => $portaldata)
            
            <?php 
            $dates = explode('-',$portaldata->portal_date);
            $date = $dates[2];
            $month = $dates[1];
            $year = $dates[0];
            $newdate= $date .'-'.$month.'-'.$year;
            ?>
            <tr>
                <td>{{ $index +1 }}</td>
                <td>{{$portaldata->portal_title}}</td>
                <td>{{$newdate}}</td>
                <td>{{$portaldata->portal_writer}}</td>
                <td>
                    <a href="{{ route('portal.edit',$portaldata->id)}}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('portal.destroy', $portaldata->id)}}" method="post" style="display:inline-block !important;">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>

                </td>

            </tr>
            @endforeach
        </tbody>
    </table>
    <div>
        @endsection