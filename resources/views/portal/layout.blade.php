<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>ABC News Portal</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>

<div class="jumbotron" style="text-align:center; padding:20px;">
  <h2>News Portal ABC - Admin Page</h2>      
  <p>Your trusted News Portal</p>
</div>
  <div class="container">
    @yield('content')
  </div>
</body>
</html>