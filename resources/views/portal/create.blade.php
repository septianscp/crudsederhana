@extends('portal.layout')

@section('content')
<style>
.uper {
    margin-top: 40px;
}
</style>
<div class="card uper">
    <div class="card-header">
        Add News
    </div>
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{ route('portal.store') }}" enctype="multipart/form-data">
            <div class="form-group">
                @csrf
                <label for="portal_title">Title:</label>
                <input type="text" class="form-control" name="portal_title" />
            </div>
            <div class="form-group">
                <label for="portal_detail">Content:</label>
                <textarea class="form-control" name="portal_detail" cols="50" rows="10" id="placeOfDeath"></textarea>
            </div>
            <div class="form-group">
                <label for="porta_date">Date:</label>
                <input class="date form-control" type="date" id="datepicker" name="portal_date">
            </div>
            <div class="form-group">
                <label for="quantity">Writer:</label>
                <select class="form-control" id="type" name="portal_writer">
                    <option value="">Select Writer....</option>
                    <option value="Septian Setia Cahya P" >Septian Setia Cahya P</option>
                    <option value="Amanda Putri" >Amanda Putri</option>
                    <option value="Jessica Nolan" >Jessica Nolan</option>
                    <option value="Annisa Safitri" >Annisa Safitri</option>
                    <option value="Wildan Nur Jam" >Wildan Nur Jam</option>
                </select>
            </div>
            <div class="form-group">
                <label for="porta_date">Picture:</label>
                <input class="form-control" type="file" name="file"/>

            </div>
            
            <button type="submit" class="btn btn-primary" style="float:right">Add</button>
        </form>
    </div>
</div>
@endsection