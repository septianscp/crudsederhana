@extends('portal.layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Share
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('portal.update', $portal->id) }}">
        @method('PATCH')
        @csrf

        <div class="form-group">
                @csrf
                <label for="portal_title">Title:</label>
                <input type="text" class="form-control" name="portal_title" value="{{ $portal->portal_title }}" />
            </div>
            <div class="form-group">
                <label for="portal_detail">Content:</label>
                <textarea class="form-control" name="portal_detail" cols="50" rows="10" >{{ $portal->portal_detail }}</textarea>
            </div>
            <div class="form-group">
                <label for="porta_date">Date:</label>
                <input class="date form-control" type="date" id="datepicker" name="portal_date" value="{{ $portal->portal_date }}">
            </div>
            <div class="form-group">
                <label for="quantity">Writer:</label>
                <select class="form-control" id="type" name="portal_writer">
                    <option value="">Select Writer....</option>
                    <option value="Septian Setia Cahya P" {{ $portal->portal_writer == 'Septian Setia Cahya P' ? 'selected' : '' }} >Septian Setia Cahya P</option>
                    <option value="Amanda Putri" {{ $portal->portal_writer == 'Amanda Putri' ? 'selected' : '' }} >Amanda Putri</option>
                    <option value="Jessica Nolan" {{ $portal->portal_writer == 'Jessica Nolan' ? 'selected' : '' }} >Jessica Nolan</option>
                    <option value="Annisa Safitri" {{ $portal->portal_writer == 'Annisa Safitri' ? 'selected' : '' }} >Annisa Safitri</option>
                    <option value="Wildan Nur Jam" {{ $portal->portal_writer == 'Wildan Nur Jam' ? 'selected' : '' }} >Wildan Nur Jam</option>
                </select>
            </div>
            <div class="form-group">
                <label for="porta_date">Picture:</label>
                <img src=" {{ asset('storage/' . $portal->image_path) }}" /> 
                <img src="{{ asset($portal->image_path) }}" alt="" class="card-img-top">
                <input class="form-control" type="file" name="file"/>

            </div>        
        <button type="submit" class="btn btn-primary" style="float:right;">Update</button>
      </form>
  </div>
</div>
@endsection