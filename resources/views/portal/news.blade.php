<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TechNews - HTML and CSS Template</title>

    <!-- favicon -->
    <link href="assets/img/favicon.png" rel=icon>

    <!-- web-fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,500' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/mobile-menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/offcanvas.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
    <!-- Bootstrap -->
    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- font-awesome -->
    <link href="assets/fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
    <!-- Mobile Menu Style -->
    <link href="assets/css/mobile-menu.css" rel="stylesheet">

    <!-- Owl carousel -->
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <link href="assets/css/owl.theme.default.min.css" rel="stylesheet">
    <!-- Theme Style -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar">
    <div id="main-wrapper">

        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>

        <div class="uc-mobile-menu-pusher">
            <div class="content-wrapper">
                <section id="header_section_wrapper" class="header_section_wrapper">
                    <div class="container">
                        <div class="header-section">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="left_section">
                                        <span class="date">
                                            {{ date('D') }} .
                                        </span>
                                        <!-- Date -->
                                        <span class="time">
                                            {{ date('d-F-Y') }}
                                        </span>
                                        <!-- Time -->
                                        <!-- Top Social Section -->
                                    </div>
                                    <!-- Left Header Section -->
                                </div>
                                <div class="col-md-4">
                                    <div class="logo">
                                        <a href="index.html"><img src="assets/img/logo.png" alt="Tech NewsLogo"></a>
                                    </div>
                                    <!-- Logo Section -->
                                </div>
                                <div class="col-md-4">
                                    <div class="right_section" onclick=" window.location.href=' {{ route('portal.index') }}'">
                                        <ul class="nav navbar-nav">
                                            <li><a  style="z-index:99;" >Admin Page</a></li>
                                        </ul>
                                        <!-- Language Section -->
                                        <!-- Search Section -->
                                    </div>
                                    <!-- Right Header Section -->
                                </div>
                            </div>
                        </div>
                        <!-- Header Section -->

                        <div class="navigation-section">
                            <nav class="navbar m-menu navbar-default">
                                <div class="container">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#navbar-collapse-1"><span class="sr-only">Toggle
                                                navigation</span>
                                            <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                                class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="#navbar-collapse-1">
                                    </div>
                                    <!-- .navbar-collapse -->
                                </div>
                                <!-- .container -->
                            </nav>
                            <!-- .nav -->
                        </div>
                        <!-- .navigation-section -->
                    </div>
                    <!-- .container -->
                </section>
                <!-- header_section_wrapper -->


                <section id="category_section" class="category_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="category_section mobile">
                                    <div class="article_title header_purple">
                                        <h2><a href="category.html" target="_self">Mobile</a></h2>
                                    </div>
                                    <!----article_title------>
                                    <div class="category_article_wrapper">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="top_article_img">
                                                    <a href="single.html" target="_self"><img class="img-responsive"
                                                            src="assets/img/cat-mobi-left-1.jpg" alt="feature-top">
                                                    </a>
                                                </div>
                                                <!----top_article_img------>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="tag purple">Mobile</span>

                                                <div class="category_article_title">
                                                    <h2><a href="single.html" target="_self">Airbnb launches
                                                            photo-centric app for iPads and Android
                                                            tablets. </a></h2>
                                                </div>
                                                <!----category_article_title------>
                                                <div class="category_article_date"><a href="#">10Aug- 2015</a>, by: <a
                                                        href="#">Eric joan</a></div>
                                                <!----category_article_date------>
                                                <div class="category_article_content">
                                                    Collaboratively administrate empowered markets via plug-and-play
                                                    networks. Dynamically procrastinate
                                                    B2C users after installed base benefits. Dramatically visualize
                                                    customer directed convergence
                                                    without revolutionary ROI.
                                                </div>
                                                <!----category_article_content------>
                                                <div class="media_social">
                                                    <span><a href="#"><i class="fa fa-share-alt"></i>424 </a>
                                                        Shares</span>
                                                    <span><i class="fa fa-comments-o"></i><a href="#">4</a>
                                                        Comments</span>
                                                </div>
                                                <!----media_social------>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Mobile News Section -->



                            </div>
                            <!-- Design News Section -->
                        </div>
                        <!-- Left Section -->


                        <!-- Popular News -->


                        <!-- Most Commented News -->


                        <!--Advertisement-->
                    </div>
                    <!-- Right Section -->

            </div>
            <!-- Row -->

        </div>
        <!-- Container -->

        </section>
        <!-- Category News Section -->



        <section id="footer_section" class="footer_section">

            <div class="footer_bottom_Section">
                <div class="container">
                    <div class="row">
                        <div class="footer">
                            <div class="col-sm-3">
                                <div class="social">
                                    <a class="icons-sm fb-ic"><i class="fa fa-facebook"></i></a>
                                    <!--Twitter-->
                                    <a class="icons-sm tw-ic"><i class="fa fa-twitter"></i></a>
                                    <!--Google +-->
                                    <a class="icons-sm inst-ic"><i class="fa fa-instagram"> </i></a>
                                    <!--Linkedin-->
                                    <a class="icons-sm tmb-ic"><i class="fa fa-tumblr"> </i></a>
                                    <!--Pinterest-->
                                    <a class="icons-sm rss-ic"><i class="fa fa-rss"> </i></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <p>&copy; Copyright 2019-ABC NEWS . Design by: <a
                                        href="https://uicookies.com">uiCookies</a>
                                </p>
                            </div>
                            <div class="col-sm-3">
                                <p>Your Trusted News</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- #content-wrapper -->

    </div>
    <!-- .offcanvas-pusher -->

    <a href="#" class="crunchify-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

    <div class="uc-mobile-menu uc-mobile-menu-effect">
        <button type="button" class="close" aria-hidden="true" data-toggle="offcanvas"
            id="uc-mobile-menu-close-btn">&times;</button>
        <div>
            <div>
                <ul id="menu">
                    <li class="active"><a href="blog.html">News</a></li>
                    <li><a href="category.html">Mobile</a></li>
                    <li><a href="blog.html">Tablet</a></li>
                    <li><a href="category.html">Gadgets</a></li>
                    <li><a href="blog.html">Camera</a></li>
                    <li><a href="category.html">Design</a></li>
                    <li class="dropdown m-menu-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">More
                            <span><i class="fa fa-angle-down"></i></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="m-menu-content">
                                    <ul class="col-sm-3">
                                        <li class="dropdown-header">Widget Haeder</li>
                                        <li><a href="#">Awesome Features</a></li>
                                        <li><a href="#">Clean Interface</a></li>
                                        <li><a href="#">Available Possibilities</a></li>
                                        <li><a href="#">Responsive Design</a></li>
                                        <li><a href="#">Pixel Perfect Graphics</a></li>
                                    </ul>
                                    <ul class="col-sm-3">
                                        <li class="dropdown-header">Widget Haeder</li>
                                        <li><a href="#">Awesome Features</a></li>
                                        <li><a href="#">Clean Interface</a></li>
                                        <li><a href="#">Available Possibilities</a></li>
                                        <li><a href="#">Responsive Design</a></li>
                                        <li><a href="#">Pixel Perfect Graphics</a></li>
                                    </ul>
                                    <ul class="col-sm-3">
                                        <li class="dropdown-header">Widget Haeder</li>
                                        <li><a href="#">Awesome Features</a></li>
                                        <li><a href="#">Clean Interface</a></li>
                                        <li><a href="#">Available Possibilities</a></li>
                                        <li><a href="#">Responsive Design</a></li>
                                        <li><a href="#">Pixel Perfect Graphics</a></li>
                                    </ul>
                                    <ul class="col-sm-3">
                                        <li class="dropdown-header">Widget Haeder</li>
                                        <li><a href="#">Awesome Features</a></li>
                                        <li><a href="#">Clean Interface</a></li>
                                        <li><a href="#">Available Possibilities</a></li>
                                        <li><a href="#">Responsive Design</a></li>
                                        <li><a href="#">Pixel Perfect Graphics</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- .uc-mobile-menu -->

    </div>
    <!-- #main-wrapper -->

    <!-- jquery Core-->
    <script src="assets/js/jquery-2.1.4.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Theme Menu -->
    <script src="assets/js/mobile-menu.js"></script>

    <!-- Owl carousel -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- Theme Script -->
    <script src="assets/js/script.js"></script>
</body>

</html>