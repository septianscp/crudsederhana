<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Portal;
use Illuminate\Support\Facades\Storage;

class PortalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $portal = Portal::all();

        return view('Portal.index', compact('portal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('portal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //]
        $request->validate([
            'portal_title'=>'required',
            'portal_detail'=> 'required',
            'portal_date' => 'required',
            'portal_writer' => 'required',
            'file' => 'file|image'

          ]);
          $file = $request->file('file');
          $name = $file->getClientOriginalName();
          $path = $file->store('public/images');

          $portal = new Portal([
            'portal_title' => $request->get('portal_title'),
            'portal_detail'=> $request->get('portal_detail'),
            'portal_date'=> $request->get('portal_date'),
            'portal_writer'=> $request->get('portal_writer'),
            'image_name'=> $name,
            'image_size'=> Storage::size($path),
            'image_type'=> pathinfo($path, PATHINFO_EXTENSION),
            'image_path'=> $path
          ]);
          $portal->save();
          return redirect('/portal')->with('success', 'News has been added');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $portal = Portal::find($id);

        return view('portal.edit', compact('portal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'portal_title'=>'required',
            'portal_detail'=> 'required',
            'portal_date' => 'required',
            'portal_writer' => 'required',
          ]);
    
          $portal = Portal::find($id);
          $portal->portal_title = $request->get('portal_title');
          $portal->portal_detail = $request->get('portal_detail');
          $portal->portal_date = $request->get('portal_date');
          $portal->portal_writer = $request->get('portal_writer');
          $portal->save();
    
          return redirect('/portal')->with('success', 'News has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $portal = Portal::find($id);
        $portal->delete();
   
        return redirect('/portal')->with('success', 'News has been deleted Successfully');
    }
}
