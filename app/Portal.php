<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portal extends Model
{
    //
    protected $fillable = [
        'portal_title','portal_detail','portal_date','portal_writer','image_name','image_size','image_type','image_path'
    ];
}
