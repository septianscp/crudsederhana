
## CRUD Portal Berita

Aplikasi ini dibuat menggunakan bahasa pemrograman php dengan framework laravel 5.7 dan menggunakan database mysql.
Tata cara instalasi dan penggunaan aplikasi ini adalah sebagai berikut:

1. Restore database terlebih dahulu, dengan cara import file db_portal.sql terlebih dahulu.
2. Setelah itu setting file .env terlebih dahulu seperti contoh dibawah

        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=db_portal
        DB_USERNAME= usernameanda
        DB_PASSWORD= passwordanda

3. Pastikan artisan sudah terinstall di laptop anda
4. Jalankan aplikasi menggunakan perintah php artisan serve
5. jika ada error lain
    "bootstrap/../vendor/autoload.php. Failed to open stream: No such file or directory. The "vendor" folder does not exist."
 gunakan perintah "composer install" pada terminal anda
 6. jia terdapat error 500, rename file .env.example menjadi .env
  - lalu generate ulang keynya menggunakan perintah "artisan key:generate" di terminal
  - lalu setting kembali file .env sesuai step 2.

7. tampilan admin ada di url : http://127.0.0.1:8000/portal
. tampilan berita ada di url : http://127.0.0.1:8000/portallive
