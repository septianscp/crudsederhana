-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2019 at 09:54 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_01_24_164752_create_portals_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portals`
--

CREATE TABLE `portals` (
  `id` int(10) UNSIGNED NOT NULL,
  `portal_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portal_detail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `portal_date` date NOT NULL,
  `portal_writer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_size` int(11) NOT NULL,
  `image_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portals`
--

INSERT INTO `portals` (`id`, `portal_title`, `portal_detail`, `portal_date`, `portal_writer`, `image_name`, `image_size`, `image_type`, `image_path`, `created_at`, `updated_at`) VALUES
(4, 'Borneo FC Dapatkan Javlon Guseynov dan Renan Silva', 'Samarinda - Borneo FC mendapatkan dua amunisi tambahan pemain asing untuk kompetisi di 2019.', '2019-01-23', 'Amanda Putri', 'gomez.jpeg', 70447, 'jpeg', 'public/images/w3BJK1nfl3zjSByfQPNyinmT5RFDYYdrDsJ54rVA.jpeg', '2019-01-24 13:45:50', '2019-01-24 13:45:50'),
(5, 'Thierry Henry Dibekukan dari Jabatan Pelatih Monaco', 'Jakarta - Thierry Henry mendekati pemecatan dari posisi pelatih AS Monaco. Monaco baru saja menangguhkan statusnya sebagai pelatih.', '2019-01-09', 'Jessica Nolan', 'henri.jpeg', 58682, 'jpeg', 'public/images/qxuuwYXrhcjNoxDBp68txhB2tToZimoIMbYQHjFQ.jpeg', '2019-01-24 13:46:36', '2019-01-24 13:46:36'),
(6, 'Pakar PBB akan Kunjungi Turki Selidiki Pembunuhan Khashoggi', 'Pelapor Khusus PBB, Agnes Callamard, disebut akan ke Turki pekan depan. Kedatangan Callamard bertujuan untuk menyelidiki kasus dugaan pembunuhan terhadap jurnalis Arab Saudi, Jamal Khashoggi.', '2019-01-26', 'Annisa Safitri', 'arab.jpeg', 96708, 'jpeg', 'public/images/Q5szmhCy2epeMbRdHQUZJQB9pzhKwUiAEvr4aEEa.jpeg', '2019-01-24 13:48:00', '2019-01-24 13:48:00'),
(7, 'Fabio Lopez Langsung Poles Taktik Borneo FC', 'Lopez telah memulai eranya di Borneo sejak Rabu (23/1/2019) lalu. Pada Kamis (24/1) kemarin, mantan pelatih akademi AS Roma ini langsung fokus ke pemahaman taktik di Stadion Segiri Samarinda.', '2019-01-10', 'Septian Setia Cahya P', 'puso.jpeg', 106645, 'jpeg', 'public/images/lyHRRd8VpispHDifXJo4twiyF6vtaewYXTKe2XlW.jpeg', '2019-01-24 13:49:02', '2019-01-24 13:49:02'),
(8, 'Keluarga Sala ke Polisi: Tolong Jangan Hentikan Pencarian!', 'Sala menumpangi pesawat yang hilang kontak di utara laut Alderney, Les Casquete, Minggu (20/1/2019) pada pukul 20.23 waktu Prancis. Pesawat berjenis Piper PA-46 itu cuma membawa dua orang.', '2019-01-25', 'Septian Setia Cahya P', 'sala.jpeg', 54634, 'jpeg', 'public/images/iR2csCVqpD4krdFemNgSIDThdcA5nJXJcWOZUrUa.jpeg', '2019-01-24 13:52:15', '2019-01-24 13:52:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portals`
--
ALTER TABLE `portals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `portals`
--
ALTER TABLE `portals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
